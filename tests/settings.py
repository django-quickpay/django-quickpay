SECRET_KEY = 'aarhus'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    # 'django-quickpay',
    'tests',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
    }
}

MIDDLEWARE_CLASSES = []


QUICKPAY_API_KEY = '1234'
QUCKPAY_PAYMENT_RECEIVED_CALLBACK_URL = 'XXX'
QUCKPAY_TOKEN_RECEIVED_CONTINUE_URL = 'XXX'
QUCKPAY_TOKEN_RECEIVED_CALLBACK_URL = 'XXX'
