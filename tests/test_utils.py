from unittest import mock
from contextlib import contextmanager

from django_quickpay.models import QuickpayTransaction, QuickpayCreditCard
from django_quickpay.signals import state_paid
from django_quickpay.utils import (
    has_status_code_and_capture, has_status_code_and_authorize,
    has_status_code_rejected
)

from django.test import TestCase


class PaymentGatewayHelperMethodsTestCase(TestCase):
    def setUp(self):
        self.operations_ok = [
            {'qp_status_code': '20000', 'type': 'authorize'},
            {'qp_status_code': '20000', 'type': 'capture'},
        ]
        self.operations_fail = [
            {'qp_status_code': '40000', 'type': 'authorize'},
        ]
        self.operations_no_capture = [
            {'qp_status_code': '40000', 'type': 'authorize'},
            {'qp_status_code': '20000', 'type': 'authorize'},
        ]
        self.operations_end_ok = [
            {'qp_status_code': '40000', 'type': 'authorize'},
            {'qp_status_code': '20000', 'type': 'authorize'},
            {'qp_status_code': '40000', 'type': 'capture'},
            {'qp_status_code': '20000', 'type': 'capture'},
        ]
        self.operations_capture_fail = [
            {'qp_status_code': '20000', 'type': 'authorize'},
            {'qp_status_code': '40000', 'type': 'capture'},
        ]

    def test_capture_success(self):
        self.assertTrue(has_status_code_and_capture(self.operations_ok))

    def test_capture_fail(self):
        self.assertFalse(has_status_code_and_capture(self.operations_fail))

    def test_capture_never_captured(self):
        """ If we fail to capture this is all we get"""
        self.assertFalse(has_status_code_and_capture(
            self.operations_no_capture))

    def test_capture_mixed_fail_capture(self):
        self.assertTrue(has_status_code_and_capture(self.operations_end_ok))

    def test_auth_success(self):
        self.assertTrue(has_status_code_and_authorize(self.operations_ok))

    def test_fail(self):
        self.assertFalse(has_status_code_and_authorize(self.operations_fail))

    def test_fail_success(self):
        self.assertTrue(has_status_code_and_authorize(
            self.operations_no_capture))

    def test_any_reject_success(self):
        self.assertFalse(has_status_code_rejected(self.operations_ok))

    def test_any_reject_fail(self):
        self.assertTrue(has_status_code_rejected(self.operations_fail))

    def test_any_reject_fail_success(self):
        self.assertTrue(has_status_code_rejected(self.operations_capture_fail))


# @contextmanager
# def catch_signal(signal):
#     """Catch django signal and return the mocked call."""
#     handler = mock.Mock()
#     signal.connect(handler)
#     yield handler
#     signal.disconnect(handler)
#
# class PaymentGatewayProcessRequestTest(TestCase):
#     def setUp(self):
#
#         continue here definining the paid transaction and catch the signal
#         self.trans_paid =
#
#     def test_should_send_signal_when_charge_succeeds(self):
#         with catch_signal(state_paid) as signal_args:
#             charge(100)
#
#         self.assertEqual(signal_args[‘total’], 100)


class ModelMethodsTestCase(TestCase):

    def test_manager_current(self):
        card1 = QuickpayCreditCard.objects.create(active=True)
        card2 = QuickpayCreditCard.objects.create(active=True)

        self.assertEqual(card2, QuickpayCreditCard.objects.get_current())
        import ipdb; ipdb.set_trace()
        dd = 1

