
# API

Get the db up running. E.g make a super user and a database:
```
emil=# CREATE USER fluf WITH PASSWORD 'fluf';
CREATE ROLE
emil=# ALTER ROLE fluf SUPERUSER;                                             
ALTER ROLE
emil=# create database fluf;
CREATE DATABASE
emil=# GRANT ALL PRIVILEGES ON DATABASE fluf TO fluf;
GRANT
```

### Assign new credit card to user
Get a credit card id to authorize:

`GET /payment_card/`

Authorize the credit card to endpoint defined here:
https://learn.quickpay.net/tech-talk/api/services/?scope=merchant#POST-cards--id-authorize---format-

Example in JS
```
<DO ME>
```

Example in Python using patched client [https://github.com/emilkjer/quickpay-python-client]
```
KEY = '<QUICKPAY API KEY>'
client = QPClient(":%s" % KEY)

card = request.get('/payment_card/')

# Setup the card details to authorize
context = {
	'card': {
	    'number': 1000000000000008,
	    'expiration': '1812', # YYMM
	    'cvd': '123',
	},
    'acquirer': 'clearhaus',
    'form': True,
}

# now prepare the card url
card_authorize_url = '/cards/{0}/authorize?synchronized'.format(card['card_id'])

# Authorize the card details
result = client.post(card_authorize_url, **context)

card = request.post('/payment_card/', {'card_id': card['card_id']})
```

When authorized mark it as activated on
`POST /payment_card/`



# Settings
`QUICKPAY_APPLY_FEES`: If the credit card service fees should be applied at the time of payment. 


## Credit
A total revamp of Niels Sandholt Busch's `https://github.com/nsb/django-quickpay` only supporting Python 3.x and never versions of Django, i.e. no backward compatibility. 
