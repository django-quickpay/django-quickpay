import random

from django.conf import settings
from django.shortcuts import render
from django.views.generic import TemplateView


class IndexView(TemplateView):
    """ Display the payment form """

    template_name = "index.html"

    def _generate_unique_id(self):
        """ Generate an unique'ish ID to send to Quickpay """
        # Removed i and o since they're ambiguous
        chars = 'abcdefghjklmnpqrstuvwxyz0123456789'
        chars += 'ABCDEFGHJKLMNPQRSTUVWXYZ'
        unique_id = ""
        length = settings.get('ORDER_ID_LENGTH', 6)
        prefix = settings.get('ORDER_ID_PREFIX', 'DJQ')

        for i in range(0, length):
            unique_id += chars[random.randint(0, len(chars)-1)]
        return prefix + unique_id

    def get(self, request, *args, **kwargs):
        order_id = self._generate_unique_id()
        amount = 1000
        currency = 'DKK'

        # Set the order details in the session
        request.session['order_id'] = order_id
        request.session['order_amount'] = amount  # remember this has to be cents
        request.session['order_currency'] = currency

        context = {
            'order_id': order_id,
            'amount': amount,
            'currency': currency,
            'merchant_id': settings.QUICKPAY_MERCHANT_ID,
            'agreement_id': settings.QUICKPAY_AGREEMENT_ID,
        }

        return render(request, self.template_name, context)


class ReceiptView(TemplateView):
    """ Display a success page for the payment """

    template_name = "receipt.html"

    def get(self, request, *args, **kwargs):

        # Get the order ID and show receipt
        order_id = request.session.get('order_id', None)

        context = {
           'order_id': order_id,
        }
        return render(request, self.template_name, context)


#TODO
class AssociateTokenView(TemplateView):
    """ This is a simple template view with a form that hit 
    the `/token_associate/` endpoint and quickpay create card 
    to associate a payment card with the user
    """
    pass

#TODO
class PerformPaymentWithTokenView(TemplateView):
    """ This is a view that demonstrates how a payment with a token can be
    performed. 
    
    Javascript driven.
    
    The view ensure the user is logged in and store the order details
    on the session. Then it hit the token payment 
    endpoint `perform-token-payment` and display the result.
    
    """
    pass
