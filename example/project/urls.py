"""Example URL Configuration

This configuration set the base views and a suggestion to where the
Django Quickpay views could live.

"""

from django_quickpay import urls as quickpay_urls

from django.conf.urls import url, include
from django.contrib import admin

from .app import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^associate_token/$', views.AssociateTokenView.as_view()),
    url(r'^$', views.IndexView.as_view()),
    url(r'^receipt/$', views.ReceiptView.as_view(), name='receipt'),
    url(r'^quickpay/', include(quickpay_urls, namespace='quickpay')),
]
