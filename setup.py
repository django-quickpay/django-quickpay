from setuptools import find_packages, setup
from django_quickpay import __version__

setup(
    name='django-quickpay',
    version=__version__,
    url='http://github.com/tinyrhino/django-quickpay',
    author='Tiny Rhino',
    author_email='contact@tinyrhino.dk',
    description="Django Integration with https://quickpay.net",
    license='BSD',
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    install_requires=[
        'Django>=1.8',
        'quickpay-api-client>=0.0.2',
        'psycopg2>=2.7.1',
    ],

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
    ],
)