from hashlib import md5, sha256
import hmac

from django import forms


class QuickpayForm(forms.Form):
    """
    When a request is composed
     Official documentation for the form located at Quickpay:
     https://learn.quickpay.net/tech-talk/payments/form/
    """

    version = forms.CharField(widget=forms.HiddenInput, initial='v10')
    merchant_id = forms.CharField(widget=forms.HiddenInput)
    agreement_id = forms.CharField(widget=forms.HiddenInput)
    order_id = forms.CharField(widget=forms.HiddenInput)
    amount = forms.IntegerField(widget=forms.HiddenInput)
    # ISO 4217 - http://www.iso.org/iso/home/standards/currency_codes.htm
    currency = forms.CharField(widget=forms.HiddenInput, initial='DKK')
    continueurl = forms.URLField(widget=forms.HiddenInput)
    cancelurl = forms.URLField(widget=forms.HiddenInput)
    checksum = forms.CharField(widget=forms.HiddenInput)

    # Non required fields for
    callbackurl = forms.URLField(widget=forms.HiddenInput, required=False)
    language = forms.CharField(widget=forms.HiddenInput, initial='da')
    autocapture = forms.IntegerField(widget=forms.HiddenInput, required=False)
    autofee = forms.IntegerField(widget=forms.HiddenInput, required=False)
    subscription = forms.IntegerField(widget=forms.HiddenInput, required=False)
    description = forms.CharField(widget=forms.HiddenInput,
                                  required=False, max_length=20)
    payment_methods = forms.CharField(widget=forms.HiddenInput, required=False)
    acquirer = forms.CharField(widget=forms.HiddenInput, required=False)
    branding_id = forms.CharField(widget=forms.HiddenInput, required=False)
    google_analytics_tracking_id = forms.CharField(widget=forms.HiddenInput,
                                                   required=False)
    google_analytics_client_id = forms.CharField(widget=forms.HiddenInput,
                                                 required=False)
    variables = forms.CharField(widget=forms.HiddenInput, required=False)
    deadline = forms.IntegerField(widget=forms.HiddenInput, required=False)
    text_on_statement = forms.CharField(widget=forms.HiddenInput,
                                        required=False)
    vat_amount = forms.IntegerField(widget=forms.HiddenInput, required=False)
    category = forms.CharField(widget=forms.HiddenInput, required=False)
    reference_title = forms.CharField(widget=forms.HiddenInput, required=False)
    product_id = forms.CharField(widget=forms.HiddenInput, required=False)
    customer_email = forms.EmailField(widget=forms.HiddenInput, required=False)

    def __init__(self, *args, **kwargs):
        api_key = kwargs.pop('api_key', None)
        super(self).__init__(*args, **kwargs)

        if api_key:
            self.set_checksum(api_key)

    def set_checksum(self, api_key):
        """ Compute the checksum and assign it to the 'checksum' field """
        data = {x.name: x.value() if x.value() else '' for x in self}
        data.pop('checksum')
        self.fields['checksum'].initial = self.sign_checksum(data, api_key)

    @staticmethod
    def sign_checksum(params, api_key):
        """Compute the checksum on ordered keys"""
        items = sorted(params.items(), key=lambda x: x[0])
        base = ' '.join([str(pair[1]) for pair in items])
        return hmac.new(
            bytes(api_key, 'utf-8'),
            bytes(base, 'utf-8'),
            sha256
        ).hexdigest()


def request_md5check(data, secret):
    """
    Computes the MD5 signature for a quickpay request.
    """

    md5pieces = (
        data['protocol'],
        data['msgtype'],
        data['merchant'],
        data['language'],
        data['ordernumber'],
        data['amount'],
        data['currency'],
        data['continueurl'],
        data['cancelurl'],

        data.get('callbackurl', ''),
        data.get('autocapture', ''),
        data.get('autofee', ''),
        data.get('cardtypelock', ''),
        data.get('description', ''),
        data.get('group', ''),
        data.get('testmode', ''),
        data.get('splitpayment', ''),
        data.get('forcemobile', ''),
        data.get('deadline', ''),
        data.get('cardhash', ''),

        secret
    )

    md5string = ''.join([str(val) for val in md5pieces if val is not None])
    return md5(md5string).hexdigest()



