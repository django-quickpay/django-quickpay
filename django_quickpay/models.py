from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import pgettext_lazy

from .managers import CreditCardManager


User = get_user_model()

QUICKPAY_STATUS_OK = '20000'
QUICKPAY_3D_SECURE_REQUIRED = '30100'
QUICKPAY_REJECT_ACQUIRER = '40000'
QUICKPAY_REQUEST_DATA_ERROR = '40001'
QUICKPAY_GATEWAY_ERROR = '50000'
QUICKPAY_COMM_ERROR_ACQUIRER = '50300'


class PaymentStatus:
    # The payment is new and ready to be handled
    NEW = 'new'

    # Payment is waiting for Payment Gateway
    PREAUTH = 'preauth'

    # Returned from Payment Gateway waiting for callback
    WAITING = 'waiting'

    # Payment confirmed and paid
    PAID = 'paid'

    # User has cancelled the transaction
    CANCELLED = 'cancelled'

    # Payment rejected by aquirer
    REJECTED = 'rejected'

    # Refund is issued
    REFUNDED = 'refunded'

    # An  error occurred
    ERROR = 'error'

    #TODO what was this again???
    INPUT = 'input'

    CHOICES = [
        (NEW, pgettext_lazy('payment status', 'Newly created')),
        (WAITING, pgettext_lazy('payment status', 'Waiting for confirmation')),
        (PREAUTH, pgettext_lazy('payment status', 'Pre-authorized')),
        (PAID, pgettext_lazy('payment status', 'Paid')),
        (CANCELLED, pgettext_lazy('payment status', 'Cancelled')),
        (REJECTED, pgettext_lazy('payment status', 'Rejected')),
        (REFUNDED, pgettext_lazy('payment status', 'Refunded')),
        (ERROR, pgettext_lazy('payment status', 'Error')),
        (INPUT, pgettext_lazy('payment status', 'Input'))]

    STATE_STALE = [NEW, WAITING, PREAUTH]


class QuickpayRequest(models.Model):
    """
    A payment request and what info is used upon the request.
    This is for audit purposes and debugging.
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # A reference back to the order made. I.e. unique order ID
    order_id = models.CharField(max_length=32, blank=True, null=True)

    # The arguments used for performing the request
    payment_args = JSONField()

    status = models.CharField(
        max_length=10,
        choices=PaymentStatus.CHOICES,
        default=PaymentStatus.NEW)
    paid_at = models.DateField(blank=True, null=True)

    class Meta:
        app_label = 'django_quickpay'


class QuickpayTransaction(models.Model):
    """ The generic transaction info coming from the payment gateway """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # A reference back to the order made. I.e. unique order ID
    order_id = models.CharField(max_length=32, blank=True, null=True)
    payment_id = models.IntegerField()
    authorized = models.BooleanField(default=False)
    captured = models.BooleanField(default=False)

    # The information received from Quickpay callback
    data_body = JSONField()
    data_meta = JSONField(null=True, blank=True)

    class Meta:
        app_label = 'django_quickpay'

    def get_quickpay_status(self):
        status = self.data_body['operations'][-1]['qp_status_code']
        message = self.data_body['operations'][-1]['qp_status_msg']
        return status, message

    def get_acquirer_status(self):
        status = self.data_body['operations'][-1]['aq_status_code']
        message = self.data_body['operations'][-1]['aq_status_msg']
        return status, message

    def get_request(self):
        """Get the quickpay payment request"""
        return QuickpayRequest.objects.get(order_id=self.order_id)


class QuickpayCreditCard(models.Model):
    """ Representation of a user """
    objects = CreditCardManager()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    expired_at = models.DateTimeField(default=None, blank=True, null=True)

    user = models.ForeignKey(User)

    # The card profile need to have added the card data before it's valid
    active = models.BooleanField(default=False)
    active_at = models.DateTimeField(default=None, blank=True, null=True)

    # If the card has been accepted by the acquirer
    accepted = models.BooleanField(default=False)
    card_id = models.IntegerField()
    merchant_id = models.IntegerField()
    acquirer = models.CharField(max_length=300, blank=True, null=True)


    test_mode = models.BooleanField(default=False)
    type = models.CharField(max_length=300)
    metadata = JSONField(default=None, null=True)

    continueurl = models.URLField()

    class Meta:
        app_label = 'django_quickpay'
