from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^get-card-fees/$',
        views.GetCardFeeView.as_view(),
        name='get_card_fees'),

    url(r'^token-is-associate/$',
        views.TokenIsAssociatedView,
        name='token_is_associate'),

    url(r'^token-associate/$',
        views.TokenAssociateView,
        name='token_associate'),

    url(r'^perform-token-payment/$',
        views.TokenPaymentView.as_view(),
        name='perform_token_payment'),

    url(r'^perform-card-payment/$',
        views.CardPaymentView.as_view(),
        name='perform_card_payment'),

    url(r'^perform-refund/$',
        views.RefundPaymentView.as_view(),
        name='perform_refund'),

    url(r'^callback/base/$',
        views.BaseQuickpayCallback.as_view(),
        name='callback_base'),
]
