from decimal import Decimal, ROUND_UP
import json
import logging; LOGGER = logging.getLogger(__name__)

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic.edit import BaseCreateView
from django.views.generic.detail import DetailView
from django.views.decorators.csrf import csrf_exempt

from .models import QuickpayTransaction, QuickpayCreditCard
from .utils import (
    create_payment_card,
    get_card_fees,
    perform_payment,
    do_payment_with_saved_token,
    mark_request_paid,
    do_refund
)


class PerformPaymentMixin:
    #TODO maybe include wrapper with requirement of logged in or similar

    def get_order_details(self, request):
        """
        Get the details on the order from the session
        It's assumed it's safer to get this from the session than the request 

        """
        order_id = request.session.get('order_id', None)
        order_amount = request.session.get('order_amount', None)
        order_currency = request.session.get('order_currency', None)

        return order_id, order_amount, order_currency


class GetCardFeeView(BaseCreateView, PerformPaymentMixin):
    """
    Given a credit card number `card_number` determine what fees to pay.
    This is dependent on which country the card is issued in, the type of card
    and the amount.
        :param request: 
        :return: 
    """
    def convert_amount(self, amount):
        TWOPLACES = Decimal(10) ** -2
        FOURPLACES = Decimal(10) ** -4
        CENTS = 100

        return Decimal(amount / CENTS).quantize(
            FOURPLACES).quantize(TWOPLACES, rounding=ROUND_UP)

    def post(self, request, *args, **kwargs):

        # Get the card number either from the query params or json body
        card_number = request.POST.get('card_number', None)
        acquirer = request.POST.get('acquirer', None)

        if card_number is None:
            body = json.loads(request.body.decode('utf8'))
            card_number = body.get('card_number', None)
            acquirer = body.get('acquirer', None)

        order_id, order_amount, order_currency = \
            self.get_order_details(request)

        #TODO maybe this is calling for a response object...
        result, success, message, status_code = get_card_fees(
            order_amount, order_currency, card_number, acquirer)

        # Ensure the response is converted from cents to point amount
        result['amount'] = self.convert_amount(result.get('amount', '0.0'))
        result['fee'] = self.convert_amount(result.get('fee', '0.0'))
        result['total'] = self.convert_amount(result.get('total', '0.0'))

        #TODO make the fees in whole numbers and not cents
        # Example on response from Quickpay
        # {'acquirer': 'clearhaus',
        #  'amount': 100,
        #  'fee': 60,
        #  'formula': 'n * 0.0145 > 60 => n * 0.0145 || => 60',
        #  'payment_method': 'visa-dk',
        #  'total': 160}

        if success:
            return JsonResponse({'status': 'OK', **result})
        else:
            return JsonResponse({'message': message}, status=status_code)


class CardPaymentView(BaseCreateView, PerformPaymentMixin):
    """Perform a payment with a temp token. The token is acquired by 
        using the Quickpay js client. 
        And the actual payment is handled by the backend.
        
        Simple JSON api endpoint to do a payment with a token
        This action is done synchronously.
        
        Assumptions:
        
        What is important is the following is on the session:
         * order_id
         * order_amount in cents
         * order_currency in 3 letter ISO

        Find the Quickpay js client here:
        https://payment.quickpay.net/embedded/v2/quickpay.js

        See card token example for the frontend here:
        https://learn.quickpay.net/tech-talk/payments/embedded/
    """

    def post(self, request, *args, **kwargs):

        # Get token from params
        card_token = request.POST.get('card_token', None)

        # Get token from a json body
        if card_token is None:
            body = json.loads(request.body.decode('utf8'))
            card_token = body.get('card_token', None)

        order_id, order_amount, order_currency = \
            self.get_order_details(request)

        success, message, status_code = perform_payment(
            card_token, order_id, order_amount, order_currency)

        #TODO associate Quickpay transaction ID
        #TODO make this response dependent on the outcome of the payment. Message/status_code/...
        if success:
            #TODO this response might need to be different
            return JsonResponse({'status': 'OK'})
        else:
            return JsonResponse({'message': message}, status=status_code)


class RefundPaymentView(LoginRequiredMixin, BaseCreateView):
    """ Refund an order based on an order_id
    
    Remark here the user has to be a staff.
    Optional parameter is the amount
    """
    def post(self, request, *args, **kwargs):
        order_id = request.POST.get('order_id', None)
        amount = request.POST.get('amount', None)

        # Do we have an order and or the user can not access the admin
        if not order_id or not \
                (request.user.is_staff or request.user.is_superuser):
            return JsonResponse(
                {'message': 'insufficient privileges'}, status=403)

        # All go let's do the refund
        success, message, status_code = do_refund(order_id, amount=amount)

        if success:
            return JsonResponse({'status': 'OK'})
        else:
            return JsonResponse({'message': message}, status=status_code)


class TokenAssociateView(LoginRequiredMixin, BaseCreateView):
    """ Create a payment card and associate it with the current user
    
    It is here assumed the user is signed in and not anonymous.
    
    Use the `card_id` to send to Quickpay in order to associate the card
    with the user.

    GET: Get a newly created card id
    POST: Mark card as active
    """

    def get(self, request, *args, **kwargs):
        card = create_payment_card(request.user)
        response = {
            'card_id': card.card_id,
            'timestamp': timezone.now().timestamp()
        }
        return JsonResponse(response)

    def post(self, request, *args, **kwargs):
        """ Mark the associated card to the user as activated at quickpay
        
        Here "activated" mean: card details are added to the card
        """
        card_id = request.POST.get('card_id', None)
        card = get_object_or_404(QuickpayCreditCard,
                                 card_id=card_id,
                                 user=request.user)

        #TODO add the expiry month/year to the card
        card.active = True
        card.save()

        response = {
            'card_id': card_id,
            'timestamp': timezone.now().timestamp()
        }
        return JsonResponse(response)


class TokenPaymentView(BaseCreateView, PerformPaymentMixin):
    """ Perform a payment with a token associated with the user
        
        Simple JSON api endpoint
       
        This action is done synchronously.
        
        Assumptions:
        
        What is important is the following is on the session:
         * order_id
         * order_amount in cents
         * order_currency in 3 letter ISO

     """
    def post(self, request, *args, **kwargs):

        order_id, order_amount, order_currency = self.get_order_details(request)

        success, message, status_code = do_payment_with_saved_token(
            request.user,
            order_id=order_id,
            amount=order_amount,
            currency=order_currency)

        #TODO make this response dependent on the outcome of the payment. Message/status_code/...
        if success:
            #TODO associate Quickpay transaction ID
            return JsonResponse({'status': True})
        else:
            return JsonResponse(
                {
                    'status': False,
                    'message': message
                },
                status=status_code
            )


class TokenIsAssociatedView(DetailView):
    """ Check if a user has associated a credit card to it's user
    
        Assumptions:
         * The user is logged in
    
    """

    def get(self, request, *args, **kwargs):
        has_active_card = QuickpayCreditCard.objects.get_user_has_active_card(
            request.user)
        return JsonResponse({'status': has_active_card})


class BaseQuickpayCallback(BaseCreateView):
    """Base view for receiving Quickpay callbacks"""
    http_method_names = ('post',)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        LOGGER.info(request.POST)
        LOGGER.info(request.META)
        LOGGER.info(request.body)


        checksum = request.META.get('HTTP_QUICKPAY_CHECKSUM_SHA256', '')
        LOGGER.info("checksum_data 1 " + checksum)

        try:
            received_json_data = json.loads(request.body.decode("utf-8-sig"))
            LOGGER.info(received_json_data)
        except Exception as error:
            LOGGER.error(error)
            received_json_data = {'order_id': 0}

        # Convert the meta data into a string dict
        meta_dict = {}
        for k, v in request.META.items():
            if isinstance(v, str):
                meta_dict[k] = v

        pay_trans = QuickpayTransaction.objects.create(
            order_id=received_json_data['order_id'],
            data_body=received_json_data,
            data_meta=meta_dict,
        )

        success, message, status_code = mark_request_paid(pay_trans.id)

        if success:
            return HttpResponse('OK')
        else:
            return HttpResponse('FAIL', status=400)

