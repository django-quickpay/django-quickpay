import json
import logging; LOGGER = logging.getLogger(__name__)

from quickpay_api_client import QPClient

from django.conf import settings
from django.utils import timezone

from .models import (
    QuickpayCreditCard, PaymentStatus, QuickpayTransaction, QUICKPAY_STATUS_OK
)
from .signals import state_paid, state_failure


CALLBACK_URL = settings.QUCKPAY_PAYMENT_RECEIVED_CALLBACK_URL
AUTO_CAPTURE = getattr(settings, 'QUICKPAY_AUTOCAPTURE', False)
API_KEY = ":{0}".format(settings.QUICKPAY_API_KEY)
CLIENT = QPClient(API_KEY)


def create_payment_card(user):
    """ Create a card and assign it to the user.

        A user can have multiple card.
    """

    # Create the card to be associated with this user
    card = CLIENT.post("/cards")
    LOGGER.debug("create_payment_card: {0}".format(card))

    #TODO do some errorhandling here if we can't create the card
    card_id = card['id']

    continue_url = settings.QUCKPAY_TOKEN_RECEIVED_CONTINUE_URL

    card_obj = QuickpayCreditCard(
        user=user, card_id=card_id, continueurl=continue_url)

    return card_obj


#TODO delete this? PCI...
def get_card_fees(amount=None, currency=None, card_number=None, acquirer=None):
    """
    To get the fees based on payment details use this endpoint
    :param amount: integer (cents)
    :param card_number: integer
    :param currency: (optional)
    :param acquirer: (optional)
    :return: Results object (TODO)
    """
    fees_url = '/fees'

    #TODO implement alternetive for if we have the card token and not a card number
    # Mark if the payment is success full
    success = False
    message = None
    status_code = 200

    # Prepare the payment
    if not currency:
        currency = getattr(settings, 'QUICKPAY_CURRENCY', 'DKK')

    LOGGER.debug("Fees for card number: {0}".format(card_number))

    data = {
        'amount': amount,
        'currency': currency,
        'card': {
            "number": card_number,
        },
        'acquirer': acquirer,
    }
    response = CLIENT.post(fees_url, **data)
    LOGGER.debug("Fee response : {0}".format(response))

    if response:
        success = True
        result = response
    else:
        result = None
        message = "did not authorize"
        status_code = 404

    return result, success, message, status_code

def do_payment_with_saved_token(user,
                                order_id=None,
                                amount=None,
                                currency=None):
    """Perform payment with stored card associated to a user

    If we have stored the credit card on a user get the current one and
    perform the payment
    """
    card = user.card.get_current()

    # Get the card token
    # See https://learn.quickpay.net/tech-talk/api/services
    token_url = '/cards/{0}/tokens'.format(card.card_id)
    token_response = CLIENT.post(token_url)
    LOGGER.debug("do_payment_with_saved_token: {0}".format(token_response))

    #TODO do error handling here if the card is expired or invalid
    card_token = token_response['token']

    # Perform payment
    status, message, status_code = perform_payment(
        card_token, order_id=order_id, amount=amount, currency=currency)

    #TODO do something here
    return status, message, status_code


#TODO do some errorhandling here
def perform_payment(card_token, order_id, amount, currency=None):
    """Perform a payment

    card_token: Token from JS
    order_id: MUST be unique to what has been send to Quickpay before
    amount: must be in cent
    currency: uppercase 3 letter ISO format, default 'DKK'

    """

    # Mark if the payment is success full
    success = False
    message = None
    status_code = 200

    # Prepare the payment
    if not currency:
        currency = getattr(settings, 'QUICKPAY_CURRENCY', 'DKK')

    payment_init = CLIENT.post(
        "/payments", order_id=order_id, currency=currency)
    LOGGER.debug("perform_payment 1: {0}".format(payment_init))
    #TODO do some error handling here otherwise this will blow up
    # Assuming everything went well move on to the actual payment
    payment_id = payment_init['id']
    LOGGER.debug("Payment ID 2: {0}".format(payment_id))

    #TODO amend with more parameters available to the Quickpay endpoint
    # Now that we have the payment id, let's do the payment
    auth_url = "/payments/{0}/authorize?synchronized".format(payment_id)
    authorize_response = CLIENT.post(
        auth_url,
        amount=amount,
        card={"token": card_token},
        callbackurl=CALLBACK_URL,
        auto_capture=AUTO_CAPTURE)

    LOGGER.debug("perform_payment 3: {0}".format(authorize_response))

    authorize_operations = authorize_response['operations']

    #TODO look at authorize_resonse['accepted']?
    is_authorized = has_status_code_and_authorize(authorize_operations)
    is_captured = False

    if is_authorized:
        # If we auto capture we check if the payment has been processed
        if AUTO_CAPTURE:
            is_captured = has_status_code_and_capture(authorize_operations)
            success = is_captured
        else:
            success = True
    else:
        #TODO do some meaningful error handling here
        message = "did not authorize"
        status_code = 404

    record_transaction(order_id,
                       payment_id,
                       authorize_response,
                       authorized=is_authorized,
                       captured=is_captured)
    #TODO implement the message in case of error
    return success, message, status_code


def do_capture(order_id):
    """ If we didn't capture the amount do it manually """
    status_code = 200
    is_captured = False
    message = None

    # Get the transaction for the authorization
    transaction = QuickpayTransaction.objects.filter(
        order_id=order_id,
        authorized=True).last()

    if transaction:
        payment_id = transaction.payment_id

        # If we didn't capture all the money do it in a call now
        amount = get_authorize_amount(transaction.data_body)
        capture_url = "/payments/{0}/capture?synchronized".format(payment_id)

        import time
        start_time = time.time()
        # capture the whole lot
        capture_response = CLIENT.post(capture_url, amount=amount)
        end_time = time.time()
        LOGGER.debug("START: {0}, END: {1}, DIFF: {2}".format(start_time, end_time, end_time-start_time))

        LOGGER.debug("do_capture: {0}".format(capture_response))

        capture_operations = capture_response['operations']
        is_captured = has_status_code_and_capture(capture_operations)
        record_transaction(order_id,
                           payment_id,
                           capture_response,
                           captured=is_captured)

    else:
        #TODO do some meaningful error handling here
        message = "did not capture"
        status_code = 404

    success = is_captured
    return success, message, status_code


def do_refund(order_id, amount=None):
    """ Util function for refunding an order ID """
    is_refunded = False
    status_code = 200
    message = None

    # Get the transaction for the authorization
    transaction_authorize = QuickpayTransaction.objects.filter(
        order_id=order_id,
        authorized=True)

    transaction_capture = QuickpayTransaction.objects.filter(
        order_id=order_id,
        captured=True)

    # If we have a capture prefer that one otherwise go for the authorization
    transaction = transaction_capture.last() \
        if transaction_capture.exists() else transaction_authorize.last()

    if transaction:

        # To compose the url we need to grab the payment_id
        payment_id = transaction.payment_id

        # If we haven't defined an amount to refund e.g. only partly set it
        if not amount:
            amount = get_authorize_amount(transaction.data_body)

        refund_url = "/payments/{0}/refund?synchronized".format(payment_id)

        refund_response = CLIENT.post(refund_url, amount=amount)
        is_refunded = refund_response['accepted']
        record_transaction(order_id, payment_id, refund_response)
    else:
        #TODO do some meaningful error handling here
        message = "did not capture"
        status_code = 404

    success = is_refunded
    return success, message, status_code


def record_transaction(
        order_id, payment_id, payment_response,
        authorized=False, captured=False):
    """Store information regarding the current transaction"""

    #TODO associate transaction with the QuickpayRequest

    transaction = QuickpayTransaction.objects.create(
        data_body=payment_response,
        order_id=order_id,
        payment_id=payment_id,
        authorized=authorized,
        captured=captured
    )

    # Payment placed, time to investigate the success
    LOGGER.debug("Payment posted ID: {0}. Transaction ID: {1}".format(
        payment_id,
        transaction.id
    ))
    return True


def get_authorize_amount(response_data):
    """Get the amount for the payment dependent on if the buyer should pay
     the payment fees or if the buyer take this on them.

     Default is YES.
    """
    should_apply_fees = getattr(settings, 'QUICKPAY_APPLY_FEES', True)
    authorize_operations = response_data['operations']
    amount_with_fee = authorize_operations[-1]['amount']

    if should_apply_fees:
        amount = amount_with_fee
    else:
        fee = response_data['fee']
        amount = amount_with_fee - fee

    return amount

def has_status_code_and_capture(operations):
    """ Check if we have OK status code and captured the money """
    return any(True for item in operations
               if str(item['qp_status_code']) == QUICKPAY_STATUS_OK
               and item['type'].lower() == 'capture')


def has_status_code_and_authorize(operations):
    """ Check if we have OK status code and pure authorize because
    then we've had a capture failure """

    return any(True for item in operations
               if str(item['qp_status_code']) == QUICKPAY_STATUS_OK
               and item['type'].lower() == 'authorize')


def has_status_code_rejected(operations):
    """ Check if we have any operations that are not type accepted """
    return any(True for item in operations
               if str(item['qp_status_code']) != QUICKPAY_STATUS_OK)


def get_status_codes(operations):
    return [item['qp_status_code'] for item in operations]


def get_and_ensure_json(payment_transaction):
    """ Ensure we have a json object from the payment gateway """
    if isinstance(payment_transaction.data_body, str):
        return json.loads(payment_transaction.data_body)
    else:
        return payment_transaction.data_body


#TODO handle if marked fraudulent
def mark_request_paid(payment_transaction_id):
    """
    Handle the callback from the payment gateway.
    This can with advantage be wrapped in a task.

    If the transaction is successful signal 'state_paid' emitted.
    If the transaction has failed a signal 'signal_failure' emitted.

    Those signals should be handled elsewhere in the code and
    acted upon accordingly. E.g. when the payment is completed
    the purchase logic can continue. Whereas if something
    went wrong, this can be handled as well.

    """

    pay_trans = QuickpayTransaction.objects.get(id=payment_transaction_id)

    # Ensure we have json on on the transaction object
    result = get_and_ensure_json(pay_trans)

    success = False
    message = None
    status_code = None
    state_change = None

    # Let's pull out status of the transactions
    operation_codes = result['operations']
    status_codes = get_status_codes(operation_codes)
    status_codes_capture = has_status_code_and_capture(operation_codes)
    status_codes_rejected = has_status_code_rejected(operation_codes)

    quickpay_request = pay_trans.get_request()

    if quickpay_request.state in PaymentStatus.STATE_STALE and \
            status_codes_capture:
        state_change = PaymentStatus.PAID
        # All OK we have our money so send the package
        success = True
        quickpay_request.state = state_change
        quickpay_request.paid_at = timezone.now()
        quickpay_request.save(update_fields=['state', 'paid_at'])

        state_paid.send(sender=quickpay_request.__class__,
                        instance=quickpay_request)

    elif status_codes_rejected:
        # Do we have any status codes not QUICKPAY_STATUS_OK, then
        # let's check if we don't have the error in the end and if so warn
        if status_codes[-1] != QUICKPAY_STATUS_OK:
            state_change = 'FAILED'
            state_failure.send(sender=quickpay_request.__class__,
                               instance=quickpay_request,
                               state=state_change)
    else:
        # it's just OK activities so do a logging
        state_change = 'SKIP'

    message = "MarkQuickpayRequestPaid: {0}: {0}".format(
        state_change, payment_transaction_id, status_codes)

    return success, message, status_code
