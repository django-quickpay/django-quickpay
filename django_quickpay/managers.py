from django.db import models
from django.utils import timezone


class CreditCardManager(models.Manager):
    def get_base_by_user(self, user):
        return super().get_queryset().filter(
            active=True,
            user=user,
            expired_at__gt=timezone.now()  # Only cards that haven't expired yet
        )

    def get_current(self, user):
        """ The current card is the latest active card """
        return self.get_base_by_user(user).order_by('created').first()

    def get_user_has_active_card(self, user):
        """ Check if the users has an associated card"""
        return self.get_base_by_user(user).exists()
