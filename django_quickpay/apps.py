from django.apps import AppConfig


class DjangoQuickpayConfig(AppConfig):
    name = 'django-quickpay'
    verbose_name = 'Django Quickpay'
